﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;


namespace WindowsFormsApp1
{

    public partial class writerForm : Form
    {

        /* form for genres */
        Form genreForm;
        /* row for connection */
        const string connectionRow = "Data Source=(localdb)\\ProjectsV13;Initial Catalog=WriterDatabase;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False;";
        /* impossibleID for exceptions */
        const int impossibleID = -65534;
        /* default genres */
        string[] defaultGenres;

        public writerForm()
        {
            InitializeComponent();
            /* default genres */
            defaultGenres = new string[3];
            defaultGenres[0] = "Realism";
            defaultGenres[1] = "Modernism";
            defaultGenres[2] = "Romanticism";
            /* make field with empty field */
            IDBox.Text = "";
            nameBox.Text = "";
            middleNameBox.Text = "";
            surnameBox.Text = "";
            mainWorkBox.Text = "";
            mainGenreBox.Text = "";
        }

        private void genreButton_Click(object sender, EventArgs e)
        {
            /* create form for genres and close form for writers */
            this.Visible = false;
            genreForm = new genreForm(this, mainGenreBox);
            genreForm.Show();
        }

        private void writerForm_Load(object sender, EventArgs e)
        {

            /* variables */
            int i, i1; bool ind; /* indexators and indicator */
            List<string> genres = new List<string>(); /* genres in the table */

            using (SqlConnection connection = new SqlConnection(connectionRow))
            {

                /* make connection */
                connection.Open();

                /* read genres from table */
                string queryRow = "select * from Genre";
                SqlCommand commReader = new SqlCommand(queryRow, connection);
                SqlDataReader reader = commReader.ExecuteReader();
                while (reader.Read())
                {
                    genres.Add(reader[1].ToString());
                }
                reader.Close();

                // to debug code
                /* i = 0;
                while (i < genres.Count())
                {
                    resultBox.Text += genres[i] + " "; 
                    i++;
                } */

                /* check if all default genres are in the table */
                i = 0;
                while (i < defaultGenres.Length)
                {
                    i1 = 0;
                    ind = false;
                    while (i1 < genres.Count())
                    {
                        /* checking */
                        if (genres[i1] == defaultGenres[i])
                        {
                            ind = true;
                        }
                        i1++;
                    }
                    /* add genre if this genre does not exist in the table */
                    if (!ind)
                    {
                        queryRow = "insert into Genre values(@genre)";
                        SqlCommand commAdd = new SqlCommand(queryRow, connection);
                        commAdd.Parameters.AddWithValue("@genre", defaultGenres[i]);
                        commAdd.ExecuteNonQuery();
                    }
                    i++;
                }

                /* read again genres from table */
                queryRow = "select * from Genre";
                commReader = new SqlCommand(queryRow, connection);
                reader = commReader.ExecuteReader();
                /* save result in box for main genres */
                while (reader.Read())
                {
                    mainGenreBox.Items.Add(reader[1].ToString());
                }
                reader.Close();

            }

        }

        private bool passLetterFormat(string row)
        {

            /* launch regular expression and check row */
            Regex regex = new Regex("^[A-Z|А-Я|І|Ї][a-z|а-я|і|ї]*$");
            Match match = regex.Match(row);
            return match.Success;

        }

        private bool getData(ref int ID, ref string name, ref string middleName, ref string surname, ref string mainWork, ref string mainGenre)
        {

            /* receive all data using boxes of form */
            name = nameBox.Text;
            middleName = middleNameBox.Text;
            surname = surnameBox.Text;
            mainWork = mainWorkBox.Text;
            mainGenre = mainGenreBox.Text;

            /* receive ID and handle exception */
            if (!addWriterButton.Checked) {
                try
                {
                    ID = Convert.ToInt32(IDBox.Text);
                }
                catch (Exception)
                {
                    /* return and show error */
                    resultBox.Text = "Id was written uncorrect (it is not number)!";
                    return false;
                }
            }

            /* chack format of name, surname and middle name */
            bool ind;
            ind = passLetterFormat(name);
            if (!ind)
            {
                resultBox.Text = "Name was written uncorerect (it is not name)!";
                return ind;
            }
            ind = passLetterFormat(middleName);
            if (!ind)
            {
                resultBox.Text = "Middle name was written uncorerect (it is not middle name)!";
                return ind;
            }
            ind = passLetterFormat(surname);
            if (!ind)
            {
                resultBox.Text = "Surname was written uncorerect (it is not surname)!";
                return ind;
            }
            return ind;

        }

        private bool getID(ref int ID)
        {

            /* receive ID and handle exception */
            if (!addWriterButton.Checked)
            {
                try
                {
                    ID = Convert.ToInt32(IDBox.Text);
                }
                catch (Exception)
                {
                    /* return and show error */
                    resultBox.Text = "Id was written uncorrect (it is not number)!";
                    return false;
                }
            }

            return true;

        }

        private void addAllParamaters(SqlCommand comm, int ID, string name, string middleName, string surname, string mainWork, int idGenre)
        {

            /* add all paramaters for request */
            comm.Parameters.Add("@name", SqlDbType.NVarChar);
            comm.Parameters["@name"].Value = name;
            comm.Parameters.Add("@middleName", SqlDbType.NVarChar);
            comm.Parameters["@middleName"].Value = middleName;
            comm.Parameters.Add("@surname", SqlDbType.NVarChar);
            comm.Parameters["@surname"].Value = surname;
            comm.Parameters.Add("@mainWork", SqlDbType.NVarChar);
            comm.Parameters["@mainWork"].Value = mainWork;
            comm.Parameters.Add("@idGenre", SqlDbType.Int);
            comm.Parameters["@idGenre"].Value = idGenre;
            /* ID is unnuccesary for adding of record */
            if (!addWriterButton.Checked)
            {
                comm.Parameters.Add("@ID", SqlDbType.Int);
                comm.Parameters["@ID"].Value = ID;
            }

        }

        private int findIDGenre(SqlConnection connection, string genre, ref bool ind)
        {

            /* make request to database */
            string queryFind = "select ID from Genre where Genre = @genre";
            SqlCommand commFind = new SqlCommand(queryFind, connection);
            commFind.Parameters.AddWithValue("@genre", genre);

            /* make and apply reader */
            SqlDataReader reader = commFind.ExecuteReader();
            reader.Read();
            int idGenre;

            /* receive id and resurn result */
            try
            {
                idGenre = Convert.ToInt32(reader[0].ToString());
            }
            catch (Exception)
            {
                idGenre = impossibleID;
                resultBox.Text = "Genre was written uncorrect (this genre does not exist in the system)!";
                ind = false;
                return idGenre;
            }
            finally
            {
                reader.Close();
            }

            ind = true;
            return idGenre;

        }

        private void writeResult(int rowsAffected)
        {

            /* write in the box for results */
            resultBox.Text = "RowsAffected: " + rowsAffected.ToString();

        }

        private void removeRecord()
        {

            /* read data to delete record */
            int ID = 0;
            bool ind = true;
            ind = getID(ref ID);

            /* return if data is uncorrect */
            if (!ind)
                return;

            using (SqlConnection connection = new SqlConnection(connectionRow))
            {

                /* make connection */
                connection.Open();

                // to debug code
                /* resultBox.Text += ID.ToString() + " ";
                resultBox.Text += name + " ";
                resultBox.Text += middleName + " ";
                resultBox.Text += surname + " ";
                resultBox.Text += mainWork + " ";
                resultBox.Text += mainGenre + " "; */

                /* delete record with such paramater */
                string queryDelete = "delete Writer where ID = @ID";
                SqlCommand commDelete = new SqlCommand(queryDelete, connection);
                commDelete.Parameters.Add("@ID", SqlDbType.Int);
                commDelete.Parameters["@ID"].Value = ID;
                int rowsAffected = commDelete.ExecuteNonQuery();
                /* write results */
                writeResult(rowsAffected);

            }

        }

        private void updateRecord()
        {

            /* read data to update record */
            int ID = 0;
            bool ind = true;
            string name = "", middleName = "", surname = "", mainWork = "", mainGenre = "";
            ind = getData(ref ID, ref name, ref middleName, ref surname, ref mainWork, ref mainGenre);

            /* return if data is uncorrect */
            if (!ind)
                return;

            using (SqlConnection connection = new SqlConnection(connectionRow))
            {

                /* make connection */
                connection.Open();
                /* find id of genre */
                int idGenre = findIDGenre(connection, mainGenre, ref ind);

                /* return if genre is uncorrect */
                if (!ind)
                    return;

                /* update record with such paramater */
                string queryUpdate = "update Writer set FirstName = @name, MiddleName = @middleName, LastName = @surname, MainWork= @mainWork, IDGenre = @idGenre where ID = @ID";
                SqlCommand commUpdate = new SqlCommand(queryUpdate, connection);
                addAllParamaters(commUpdate, ID, name, middleName, surname, mainWork, idGenre);
                int rowsAffected = commUpdate.ExecuteNonQuery();
                /* write results */
                writeResult(rowsAffected);

            }

        }

        private void addRecord()
        {

            /* read data to add record */
            int ID = 0;
            bool ind = true;
            string name = "", middleName = "", surname = "", mainWork = "", mainGenre = "";
            ind = getData(ref ID, ref name, ref middleName, ref surname, ref mainWork, ref mainGenre);

            /* return if data is uncorrect */
            if (!ind)
                return;

            using (SqlConnection connection = new SqlConnection(connectionRow))
            {

                /* make connection */
                connection.Open();
                /* find id of genre */
                int idGenre = findIDGenre(connection, mainGenre, ref ind);

                /* return if genre is uncorrect */
                if (!ind)
                    return;

                /* update record with such paramater */
                string queryAdd = "insert into Writer values(@name, @middleName, @surname, @mainWork, @idGenre)";
                SqlCommand commAdd = new SqlCommand(queryAdd, connection);
                addAllParamaters(commAdd, ID, name, middleName, surname, mainWork, idGenre);
                int rowsAffected = commAdd.ExecuteNonQuery();
                /* write results */
                writeResult(rowsAffected);

            }

        }

        private void getRecord()
        {

            using (SqlConnection connection = new SqlConnection(connectionRow))
            {

                /* make connection */
                connection.Open();

                /* get record using ID */
                if (IDCheckBox.Checked)
                {

                    /* read id to get record */
                    int ID = 0;
                    bool ind = getID(ref ID);

                    /* return if data is uncorrect */
                    if (!ind)
                        return;

                    /* make requast to get information */
                    string queryGet = "select * from Writer where ID = @ID";
                    SqlCommand commGet = new SqlCommand(queryGet, connection);
                    commGet.Parameters.Add("@ID", SqlDbType.Int);
                    commGet.Parameters["@ID"].Value = ID;
                    SqlDataReader reader = commGet.ExecuteReader();

                    /* write results */
                    resultBox.Text = string.Format("{0,-10} {1,-15} {2,-15} {3,-15} {4,-25} {5,-10}\r\n", "ID", "FirstName", "MiddleName", "LastName", "MainWork", "IDGenre");
                    while (reader.Read())
                    {
                        resultBox.Text += string.Format("{0,-10} {1,-15} {2,-15} {3,-15} {4,-25} {5,-10}\r\n", reader[0].ToString(), reader[1].ToString(), reader[2].ToString(), reader[3].ToString(), reader[4].ToString(), reader[5].ToString());
                    }

                }
                /* get record using genre */
                else if (GenreCheckBox.Checked)
                {

                    /* read genre to get record */
                    string mainGenre = mainGenreBox.Text;
                    bool ind = true;
                    int idGenre = findIDGenre(connection, mainGenre, ref ind);

                    /* return if data is uncorrect */
                    if (!ind)
                        return;

                    /* make requast to get information */
                    string queryGet = "select * from Writer where IDGenre = @idGenre";
                    SqlCommand commGet = new SqlCommand(queryGet, connection);
                    commGet.Parameters.Add("@IDGenre", SqlDbType.Int);
                    commGet.Parameters["@IDGenre"].Value = idGenre;
                    SqlDataReader reader = commGet.ExecuteReader();

                    /* write results */
                    resultBox.Text = string.Format("{0,-10} {1,-15} {2,-15} {3,-15} {4,-25} {5,-10}\r\n", "ID", "FirstName", "MiddleName", "LastName", "MainWork", "IDGenre");
                    while (reader.Read())
                    {
                        resultBox.Text += string.Format("{0,-10} {1,-15} {2,-15} {3,-15} {4,-25} {5,-10}\r\n", reader[0].ToString(), reader[1].ToString(), reader[2].ToString(), reader[3].ToString(), reader[4].ToString(), reader[5].ToString());
                    }

                }
                /* make joining of two tables */
                else
                {

                    /* make requast to get information */
                    string queryGet = "select * from Writer join Genre on Writer.IDGenre = Genre.ID ";
                    SqlCommand commGet = new SqlCommand(queryGet, connection);
                    SqlDataReader reader = commGet.ExecuteReader();

                    /* write results */
                    resultBox.Text = string.Format("{0,-10} {1,-15} {2,-15} {3,-15} {4,-25} {5,-10} {6,-15}\r\n", "ID", "FirstName", "MiddleName", "LastName", "MainWork", "IDGenre", "Genre");
                    while (reader.Read())
                    {
                        resultBox.Text += string.Format("{0,-10} {1,-15} {2,-15} {3,-15} {4,-25} {5,-10} {6,-15}\r\n", reader[0].ToString(), reader[1].ToString(), reader[2].ToString(), reader[3].ToString(), reader[4].ToString(), reader[5].ToString(), reader[7].ToString());
                    }

                }

            }

        }

        private void requestButton_Click(object sender, EventArgs e)
        {

            if (removeWriterButton.Checked)
            {
                /* remove record */
                removeRecord();
            }
            else if (updateWriterButton.Checked)
            {
                /* update record */
                updateRecord();
            }
            else if (addWriterButton.Checked)
            {
                /* add record */
                addRecord();
            }
            else if (getWriterButton.Checked)
            {
                /* get record */
                getRecord();
            }

        }

        private void addWriterButton_CheckedChanged(object sender, EventArgs e)
        {
            /* change mode of IDBox for adding of record */
            if (addWriterButton.Checked)
                IDBox.Enabled = false;
            else
                IDBox.Enabled = true;
        }

        private void removeWriterButton_CheckedChanged(object sender, EventArgs e)
        {
            /* change mode of all boxes without IDbox for removing of record */
            if (removeWriterButton.Checked)
            {
                nameBox.Enabled = false;
                surnameBox.Enabled = false;
                middleNameBox.Enabled = false;
                mainWorkBox.Enabled = false;
                mainGenreBox.Enabled = false;
            }
            else
            {
                nameBox.Enabled = true;
                surnameBox.Enabled = true;
                middleNameBox.Enabled = true;
                mainWorkBox.Enabled = true;
                mainGenreBox.Enabled = true;
            }
        }
    }

}
