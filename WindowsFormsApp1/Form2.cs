﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace WindowsFormsApp1
{

    public partial class genreForm : Form
    {

        /* form of writers to change window */
        Form writerForm;
        /* box of father form for main genre  */
        ComboBox mainGenreWriterBox;
        /* row for connection */
        const string connectionRow = "Data Source=(localdb)\\ProjectsV13;Initial Catalog=WriterDatabase;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False;";

        public genreForm(Form fatherForm, ComboBox mainGenreBox)
        {
            /* save father form and box for main genre (writerForm) */
            writerForm = fatherForm;
            mainGenreWriterBox = mainGenreBox;
            InitializeComponent();
        }

        private bool getData(ref int ID, ref string genre)
        {

            /* receive all data using boxes of form */
            genre = genreBox.Text;

            /* receive ID and handle exception */
            if (!addGenreButton.Checked)
            {
                try
                {
                    ID = Convert.ToInt32(IDBox.Text);
                }
                catch (Exception)
                {
                    /* return and show error */
                    resultBox.Text = "Id was written uncorrect (it is not number)!";
                    return false;
                }
            }

            return true;

        }

        private void addAllParamaters(SqlCommand comm, int ID, string genre)
        {

            /* add all paramaters for request */
            comm.Parameters.Add("@genre", SqlDbType.NVarChar);
            comm.Parameters["@genre"].Value = genre;
            /* ID is unnuccesary for adding of record */
            if (!addGenreButton.Checked)
            {
                comm.Parameters.Add("@ID", SqlDbType.Int);
                comm.Parameters["@ID"].Value = ID;
            }

        }

        private void writeResult(int rowsAffected)
        {

            /* write in the box for results */
            resultBox.Text = "RowsAffected: " + rowsAffected.ToString();

        }

        private void removeRecord()
        {

            /* read data to delete record */
            int ID = 0;
            string genre = "";
            bool ind = getData(ref ID, ref genre);

            /* return if data is uncorrect */
            if (!ind)
                return;

            using (SqlConnection connection = new SqlConnection(connectionRow))
            {

                /* make connection */
                connection.Open();

                /* delete record with such paramater */
                string queryDelete = "delete Genre where ID = @ID";
                SqlCommand commDelete = new SqlCommand(queryDelete, connection);
                commDelete.Parameters.Add("@ID", SqlDbType.Int);
                commDelete.Parameters["@ID"].Value = ID;
                int rowsAffected = commDelete.ExecuteNonQuery();
                /* write results */
                writeResult(rowsAffected);

            }

        }

        private void updateRecord()
        {

            /* read data to add record */
            int ID = 0;
            string genre = "";
            bool ind = getData(ref ID, ref genre);

            /* return if data is uncorrect */
            if (!ind)
                return;

            using (SqlConnection connection = new SqlConnection(connectionRow))
            {

                /* make connection */
                connection.Open();
                /* update record with such paramater */
                string queryUpdate = "update Genre set Genre = @genre where ID = @ID";
                SqlCommand commUpdate = new SqlCommand(queryUpdate, connection);
                addAllParamaters(commUpdate, ID, genre);
                int rowsAffected = commUpdate.ExecuteNonQuery();
                /* write results */
                writeResult(rowsAffected);

            }

        }

        private void addRecord()
        {

            /* read data to add record */
            int ID = 0;
            string genre = "";
            bool ind = getData(ref ID, ref genre);

            /* return if data is uncorrect */
            if (!ind)
                return;

            using (SqlConnection connection = new SqlConnection(connectionRow))
            {

                /* make connection */
                connection.Open();
                /* update record with such paramater */
                string queryAdd = "insert into Genre values(@genre)";
                SqlCommand commAdd = new SqlCommand(queryAdd, connection);
                addAllParamaters(commAdd, ID, genre);
                int rowsAffected = commAdd.ExecuteNonQuery();
                /* write results */
                writeResult(rowsAffected);

            }

        }

        private void getRecord()
        {

            using (SqlConnection connection = new SqlConnection(connectionRow))
            {

                /* make connection */
                connection.Open();

                /* make requast to get information */
                string queryGet = "select * from Genre ";
                SqlCommand commGet = new SqlCommand(queryGet, connection);
                SqlDataReader reader = commGet.ExecuteReader();

                /* write results */
                resultBox.Text = string.Format("{0, -15}  {1, -30}\r\n", "ID", "Genre");
                while (reader.Read())
                {
                    resultBox.Text += string.Format("{0, -15}  {1, -30}\r\n", reader[0].ToString(), reader[1].ToString());
                }

            }

        }

        private void requestButton_Click(object sender, EventArgs e)
        {
            if (removeGenreButton.Checked)
            {
                /* remove record */
                removeRecord();
            }
            else if (updateGenreButton.Checked)
            {
                /* update record */
                updateRecord();
            }
            else if (addGenreButton.Checked)
            {
                /* add record */
                addRecord();
            }
            else if (getGenreButton.Checked)
            {
                /* get record */
                getRecord();
            }
        }

        private void addGenreButton_CheckedChanged(object sender, EventArgs e)
        {
            /* change mode of IDBox for adding of record */
            if (addGenreButton.Checked)
                IDBox.Enabled = false;
            else
                IDBox.Enabled = true;
        }

        private void removeGenreButton_CheckedChanged(object sender, EventArgs e)
        {
            /* change mode of genreBox for removing of record */
            if (removeGenreButton.Checked)
                genreBox.Enabled = false;
            else
                genreBox.Enabled = true;
        }

        private void saveResults()
        {
            /* save new genres in app */
            using (SqlConnection connection = new SqlConnection(connectionRow))
            {

                /* make connection */
                connection.Open();

                /* delete record with such paramater */
                string queryGet = "select * from Genre";
                SqlCommand commGet = new SqlCommand(queryGet, connection);
                SqlDataReader reader = commGet.ExecuteReader();

                /* save results in box for main genres */
                mainGenreWriterBox.Items.Clear();
                while (reader.Read())
                {
                    mainGenreWriterBox.Items.Add(reader[1].ToString());
                }

            }
        }

        private void genreForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            /* save results */
            saveResults();
            /* make visible father form */
            writerForm.Visible = true;
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            /* save results */
            saveResults();
            /* show father form and close this form */
            writerForm.Visible = true;
            this.Visible = false;
        }

    }

}
