﻿namespace WindowsFormsApp1
{
    partial class genreForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(genreForm));
            this.choiceActionBox = new System.Windows.Forms.GroupBox();
            this.removeGenreButton = new System.Windows.Forms.RadioButton();
            this.updateGenreButton = new System.Windows.Forms.RadioButton();
            this.addGenreButton = new System.Windows.Forms.RadioButton();
            this.getGenreButton = new System.Windows.Forms.RadioButton();
            this.IDLabel = new System.Windows.Forms.Label();
            this.IDBox = new System.Windows.Forms.TextBox();
            this.genreLabel = new System.Windows.Forms.Label();
            this.genreBox = new System.Windows.Forms.TextBox();
            this.requestButton = new System.Windows.Forms.Button();
            this.backButton = new System.Windows.Forms.Button();
            this.resultBox = new System.Windows.Forms.TextBox();
            this.choiceActionBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // choiceActionBox
            // 
            this.choiceActionBox.Controls.Add(this.removeGenreButton);
            this.choiceActionBox.Controls.Add(this.updateGenreButton);
            this.choiceActionBox.Controls.Add(this.addGenreButton);
            this.choiceActionBox.Controls.Add(this.getGenreButton);
            this.choiceActionBox.Location = new System.Drawing.Point(12, 12);
            this.choiceActionBox.Name = "choiceActionBox";
            this.choiceActionBox.Size = new System.Drawing.Size(236, 149);
            this.choiceActionBox.TabIndex = 1;
            this.choiceActionBox.TabStop = false;
            // 
            // removeGenreButton
            // 
            this.removeGenreButton.AutoSize = true;
            this.removeGenreButton.Location = new System.Drawing.Point(16, 116);
            this.removeGenreButton.Name = "removeGenreButton";
            this.removeGenreButton.Size = new System.Drawing.Size(146, 21);
            this.removeGenreButton.TabIndex = 3;
            this.removeGenreButton.TabStop = true;
            this.removeGenreButton.Text = "Видалення жанру";
            this.removeGenreButton.UseVisualStyleBackColor = true;
            this.removeGenreButton.CheckedChanged += new System.EventHandler(this.removeGenreButton_CheckedChanged);
            // 
            // updateGenreButton
            // 
            this.updateGenreButton.AutoSize = true;
            this.updateGenreButton.Location = new System.Drawing.Point(16, 88);
            this.updateGenreButton.Name = "updateGenreButton";
            this.updateGenreButton.Size = new System.Drawing.Size(147, 21);
            this.updateGenreButton.TabIndex = 2;
            this.updateGenreButton.TabStop = true;
            this.updateGenreButton.Text = "Оновлення жанру";
            this.updateGenreButton.UseVisualStyleBackColor = true;
            // 
            // addGenreButton
            // 
            this.addGenreButton.AutoSize = true;
            this.addGenreButton.Location = new System.Drawing.Point(16, 60);
            this.addGenreButton.Name = "addGenreButton";
            this.addGenreButton.Size = new System.Drawing.Size(147, 21);
            this.addGenreButton.TabIndex = 1;
            this.addGenreButton.TabStop = true;
            this.addGenreButton.Text = "Додавання жанру";
            this.addGenreButton.UseVisualStyleBackColor = true;
            this.addGenreButton.CheckedChanged += new System.EventHandler(this.addGenreButton_CheckedChanged);
            // 
            // getGenreButton
            // 
            this.getGenreButton.AutoSize = true;
            this.getGenreButton.Location = new System.Drawing.Point(16, 32);
            this.getGenreButton.Name = "getGenreButton";
            this.getGenreButton.Size = new System.Drawing.Size(148, 21);
            this.getGenreButton.TabIndex = 0;
            this.getGenreButton.TabStop = true;
            this.getGenreButton.Text = "Отримання жанру";
            this.getGenreButton.UseVisualStyleBackColor = true;
            // 
            // IDLabel
            // 
            this.IDLabel.AutoSize = true;
            this.IDLabel.Location = new System.Drawing.Point(12, 164);
            this.IDLabel.Name = "IDLabel";
            this.IDLabel.Size = new System.Drawing.Size(21, 17);
            this.IDLabel.TabIndex = 5;
            this.IDLabel.Text = "ID";
            // 
            // IDBox
            // 
            this.IDBox.Location = new System.Drawing.Point(12, 184);
            this.IDBox.Name = "IDBox";
            this.IDBox.Size = new System.Drawing.Size(125, 22);
            this.IDBox.TabIndex = 6;
            // 
            // genreLabel
            // 
            this.genreLabel.AutoSize = true;
            this.genreLabel.Location = new System.Drawing.Point(151, 164);
            this.genreLabel.Name = "genreLabel";
            this.genreLabel.Size = new System.Drawing.Size(45, 17);
            this.genreLabel.TabIndex = 7;
            this.genreLabel.Text = "Жанр";
            // 
            // genreBox
            // 
            this.genreBox.Location = new System.Drawing.Point(154, 184);
            this.genreBox.Name = "genreBox";
            this.genreBox.Size = new System.Drawing.Size(167, 22);
            this.genreBox.TabIndex = 8;
            // 
            // requestButton
            // 
            this.requestButton.Location = new System.Drawing.Point(12, 212);
            this.requestButton.Name = "requestButton";
            this.requestButton.Size = new System.Drawing.Size(162, 42);
            this.requestButton.TabIndex = 9;
            this.requestButton.Text = "Виконати запит";
            this.requestButton.UseVisualStyleBackColor = true;
            this.requestButton.Click += new System.EventHandler(this.requestButton_Click);
            // 
            // backButton
            // 
            this.backButton.Location = new System.Drawing.Point(181, 212);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(140, 42);
            this.backButton.TabIndex = 11;
            this.backButton.Text = "Назад";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // resultBox
            // 
            this.resultBox.Font = new System.Drawing.Font("Consolas", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.resultBox.Location = new System.Drawing.Point(12, 260);
            this.resultBox.Multiline = true;
            this.resultBox.Name = "resultBox";
            this.resultBox.Size = new System.Drawing.Size(510, 196);
            this.resultBox.TabIndex = 12;
            // 
            // genreForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(534, 470);
            this.Controls.Add(this.resultBox);
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.requestButton);
            this.Controls.Add(this.genreBox);
            this.Controls.Add(this.genreLabel);
            this.Controls.Add(this.IDBox);
            this.Controls.Add(this.IDLabel);
            this.Controls.Add(this.choiceActionBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "genreForm";
            this.Text = "Літературні жанри";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.genreForm_FormClosing);
            this.choiceActionBox.ResumeLayout(false);
            this.choiceActionBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox choiceActionBox;
        private System.Windows.Forms.RadioButton removeGenreButton;
        private System.Windows.Forms.RadioButton updateGenreButton;
        private System.Windows.Forms.RadioButton addGenreButton;
        private System.Windows.Forms.RadioButton getGenreButton;
        private System.Windows.Forms.Label IDLabel;
        private System.Windows.Forms.TextBox IDBox;
        private System.Windows.Forms.Label genreLabel;
        private System.Windows.Forms.TextBox genreBox;
        private System.Windows.Forms.Button requestButton;
        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.TextBox resultBox;
    }
}