﻿namespace WindowsFormsApp1
{
    partial class writerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(writerForm));
            this.choiceActionBox = new System.Windows.Forms.GroupBox();
            this.removeWriterButton = new System.Windows.Forms.RadioButton();
            this.updateWriterButton = new System.Windows.Forms.RadioButton();
            this.addWriterButton = new System.Windows.Forms.RadioButton();
            this.getWriterButton = new System.Windows.Forms.RadioButton();
            this.IDLabel = new System.Windows.Forms.Label();
            this.IDBox = new System.Windows.Forms.TextBox();
            this.nameLabel = new System.Windows.Forms.Label();
            this.nameBox = new System.Windows.Forms.TextBox();
            this.middleNameLabel = new System.Windows.Forms.Label();
            this.middleNameBox = new System.Windows.Forms.TextBox();
            this.surnameLabel = new System.Windows.Forms.Label();
            this.surnameBox = new System.Windows.Forms.TextBox();
            this.mainWorkLabel = new System.Windows.Forms.Label();
            this.mainWorkBox = new System.Windows.Forms.TextBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.mainGenreBox = new System.Windows.Forms.ComboBox();
            this.requestButton = new System.Windows.Forms.Button();
            this.genreButton = new System.Windows.Forms.Button();
            this.resultBox = new System.Windows.Forms.TextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.IDCheckBox = new System.Windows.Forms.CheckBox();
            this.GenreCheckBox = new System.Windows.Forms.CheckBox();
            this.mainGenreLabel = new System.Windows.Forms.Label();
            this.choiceActionBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // choiceActionBox
            // 
            this.choiceActionBox.Controls.Add(this.removeWriterButton);
            this.choiceActionBox.Controls.Add(this.updateWriterButton);
            this.choiceActionBox.Controls.Add(this.addWriterButton);
            this.choiceActionBox.Controls.Add(this.getWriterButton);
            this.choiceActionBox.Location = new System.Drawing.Point(11, 12);
            this.choiceActionBox.Name = "choiceActionBox";
            this.choiceActionBox.Size = new System.Drawing.Size(236, 176);
            this.choiceActionBox.TabIndex = 0;
            this.choiceActionBox.TabStop = false;
            // 
            // removeWriterButton
            // 
            this.removeWriterButton.AutoSize = true;
            this.removeWriterButton.Location = new System.Drawing.Point(16, 116);
            this.removeWriterButton.Name = "removeWriterButton";
            this.removeWriterButton.Size = new System.Drawing.Size(192, 21);
            this.removeWriterButton.TabIndex = 3;
            this.removeWriterButton.TabStop = true;
            this.removeWriterButton.Text = "Видалення письменника";
            this.removeWriterButton.UseVisualStyleBackColor = true;
            this.removeWriterButton.CheckedChanged += new System.EventHandler(this.removeWriterButton_CheckedChanged);
            // 
            // updateWriterButton
            // 
            this.updateWriterButton.AutoSize = true;
            this.updateWriterButton.Location = new System.Drawing.Point(16, 88);
            this.updateWriterButton.Name = "updateWriterButton";
            this.updateWriterButton.Size = new System.Drawing.Size(193, 21);
            this.updateWriterButton.TabIndex = 2;
            this.updateWriterButton.TabStop = true;
            this.updateWriterButton.Text = "Оновлення письменника";
            this.updateWriterButton.UseVisualStyleBackColor = true;
            // 
            // addWriterButton
            // 
            this.addWriterButton.AutoSize = true;
            this.addWriterButton.Location = new System.Drawing.Point(16, 60);
            this.addWriterButton.Name = "addWriterButton";
            this.addWriterButton.Size = new System.Drawing.Size(193, 21);
            this.addWriterButton.TabIndex = 1;
            this.addWriterButton.TabStop = true;
            this.addWriterButton.Text = "Додавання письменника";
            this.addWriterButton.UseVisualStyleBackColor = true;
            this.addWriterButton.CheckedChanged += new System.EventHandler(this.addWriterButton_CheckedChanged);
            // 
            // getWriterButton
            // 
            this.getWriterButton.AutoSize = true;
            this.getWriterButton.Location = new System.Drawing.Point(16, 32);
            this.getWriterButton.Name = "getWriterButton";
            this.getWriterButton.Size = new System.Drawing.Size(194, 21);
            this.getWriterButton.TabIndex = 0;
            this.getWriterButton.TabStop = true;
            this.getWriterButton.Text = "Отримання письменника";
            this.getWriterButton.UseVisualStyleBackColor = true;
            // 
            // IDLabel
            // 
            this.IDLabel.AutoSize = true;
            this.IDLabel.Location = new System.Drawing.Point(9, 191);
            this.IDLabel.Name = "IDLabel";
            this.IDLabel.Size = new System.Drawing.Size(21, 17);
            this.IDLabel.TabIndex = 4;
            this.IDLabel.Text = "ID";
            // 
            // IDBox
            // 
            this.IDBox.Location = new System.Drawing.Point(11, 211);
            this.IDBox.Name = "IDBox";
            this.IDBox.Size = new System.Drawing.Size(99, 22);
            this.IDBox.TabIndex = 5;
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(125, 191);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(31, 17);
            this.nameLabel.TabIndex = 6;
            this.nameLabel.Text = "Ім\'я";
            // 
            // nameBox
            // 
            this.nameBox.Location = new System.Drawing.Point(128, 211);
            this.nameBox.Name = "nameBox";
            this.nameBox.Size = new System.Drawing.Size(119, 22);
            this.nameBox.TabIndex = 7;
            // 
            // middleNameLabel
            // 
            this.middleNameLabel.AutoSize = true;
            this.middleNameLabel.Location = new System.Drawing.Point(265, 191);
            this.middleNameLabel.Name = "middleNameLabel";
            this.middleNameLabel.Size = new System.Drawing.Size(86, 17);
            this.middleNameLabel.TabIndex = 8;
            this.middleNameLabel.Text = "По-батькові";
            // 
            // middleNameBox
            // 
            this.middleNameBox.Location = new System.Drawing.Point(268, 211);
            this.middleNameBox.Name = "middleNameBox";
            this.middleNameBox.Size = new System.Drawing.Size(126, 22);
            this.middleNameBox.TabIndex = 9;
            // 
            // surnameLabel
            // 
            this.surnameLabel.AutoSize = true;
            this.surnameLabel.Location = new System.Drawing.Point(413, 191);
            this.surnameLabel.Name = "surnameLabel";
            this.surnameLabel.Size = new System.Drawing.Size(70, 17);
            this.surnameLabel.TabIndex = 10;
            this.surnameLabel.Text = "Прізвище";
            // 
            // surnameBox
            // 
            this.surnameBox.Location = new System.Drawing.Point(414, 211);
            this.surnameBox.Name = "surnameBox";
            this.surnameBox.Size = new System.Drawing.Size(135, 22);
            this.surnameBox.TabIndex = 11;
            // 
            // mainWorkLabel
            // 
            this.mainWorkLabel.AutoSize = true;
            this.mainWorkLabel.Location = new System.Drawing.Point(563, 191);
            this.mainWorkLabel.Name = "mainWorkLabel";
            this.mainWorkLabel.Size = new System.Drawing.Size(116, 17);
            this.mainWorkLabel.TabIndex = 12;
            this.mainWorkLabel.Text = "Основна робота";
            // 
            // mainWorkBox
            // 
            this.mainWorkBox.Location = new System.Drawing.Point(566, 211);
            this.mainWorkBox.Name = "mainWorkBox";
            this.mainWorkBox.Size = new System.Drawing.Size(163, 22);
            this.mainWorkBox.TabIndex = 13;
            // 
            // mainGenreBox
            // 
            this.mainGenreBox.FormattingEnabled = true;
            this.mainGenreBox.Location = new System.Drawing.Point(746, 211);
            this.mainGenreBox.Name = "mainGenreBox";
            this.mainGenreBox.Size = new System.Drawing.Size(169, 24);
            this.mainGenreBox.TabIndex = 15;
            // 
            // requestButton
            // 
            this.requestButton.Location = new System.Drawing.Point(11, 239);
            this.requestButton.Name = "requestButton";
            this.requestButton.Size = new System.Drawing.Size(202, 41);
            this.requestButton.TabIndex = 17;
            this.requestButton.Text = "Виконати запит";
            this.requestButton.UseVisualStyleBackColor = true;
            this.requestButton.Click += new System.EventHandler(this.requestButton_Click);
            // 
            // genreButton
            // 
            this.genreButton.Location = new System.Drawing.Point(230, 239);
            this.genreButton.Name = "genreButton";
            this.genreButton.Size = new System.Drawing.Size(221, 41);
            this.genreButton.TabIndex = 18;
            this.genreButton.Text = "Основні жанри";
            this.genreButton.UseVisualStyleBackColor = true;
            this.genreButton.Click += new System.EventHandler(this.genreButton_Click);
            // 
            // resultBox
            // 
            this.resultBox.Font = new System.Drawing.Font("Consolas", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resultBox.Location = new System.Drawing.Point(11, 286);
            this.resultBox.Multiline = true;
            this.resultBox.Name = "resultBox";
            this.resultBox.Size = new System.Drawing.Size(904, 236);
            this.resultBox.TabIndex = 19;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // IDCheckBox
            // 
            this.IDCheckBox.AutoSize = true;
            this.IDCheckBox.Location = new System.Drawing.Point(308, 23);
            this.IDCheckBox.Name = "IDCheckBox";
            this.IDCheckBox.Size = new System.Drawing.Size(132, 21);
            this.IDCheckBox.TabIndex = 20;
            this.IDCheckBox.Text = "Отримати за ID";
            this.IDCheckBox.UseVisualStyleBackColor = true;
            // 
            // GenreCheckBox
            // 
            this.GenreCheckBox.AutoSize = true;
            this.GenreCheckBox.Location = new System.Drawing.Point(457, 23);
            this.GenreCheckBox.Name = "GenreCheckBox";
            this.GenreCheckBox.Size = new System.Drawing.Size(173, 21);
            this.GenreCheckBox.TabIndex = 21;
            this.GenreCheckBox.Text = "Знаходити за жанром";
            this.GenreCheckBox.UseVisualStyleBackColor = true;
            // 
            // mainGenreLabel
            // 
            this.mainGenreLabel.AutoSize = true;
            this.mainGenreLabel.Location = new System.Drawing.Point(743, 191);
            this.mainGenreLabel.Name = "mainGenreLabel";
            this.mainGenreLabel.Size = new System.Drawing.Size(110, 17);
            this.mainGenreLabel.TabIndex = 14;
            this.mainGenreLabel.Text = "Основний жанр";
            // 
            // writerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(950, 531);
            this.Controls.Add(this.GenreCheckBox);
            this.Controls.Add(this.IDCheckBox);
            this.Controls.Add(this.resultBox);
            this.Controls.Add(this.genreButton);
            this.Controls.Add(this.requestButton);
            this.Controls.Add(this.mainGenreBox);
            this.Controls.Add(this.mainGenreLabel);
            this.Controls.Add(this.mainWorkBox);
            this.Controls.Add(this.mainWorkLabel);
            this.Controls.Add(this.surnameBox);
            this.Controls.Add(this.surnameLabel);
            this.Controls.Add(this.middleNameBox);
            this.Controls.Add(this.middleNameLabel);
            this.Controls.Add(this.nameBox);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.IDBox);
            this.Controls.Add(this.IDLabel);
            this.Controls.Add(this.choiceActionBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "writerForm";
            this.Text = "Письменники світу";
            this.Load += new System.EventHandler(this.writerForm_Load);
            this.choiceActionBox.ResumeLayout(false);
            this.choiceActionBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox choiceActionBox;
        private System.Windows.Forms.RadioButton removeWriterButton;
        private System.Windows.Forms.RadioButton updateWriterButton;
        private System.Windows.Forms.RadioButton addWriterButton;
        private System.Windows.Forms.RadioButton getWriterButton;
        private System.Windows.Forms.Label IDLabel;
        private System.Windows.Forms.TextBox IDBox;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.TextBox nameBox;
        private System.Windows.Forms.Label middleNameLabel;
        private System.Windows.Forms.TextBox middleNameBox;
        private System.Windows.Forms.Label surnameLabel;
        private System.Windows.Forms.TextBox surnameBox;
        private System.Windows.Forms.Label mainWorkLabel;
        private System.Windows.Forms.TextBox mainWorkBox;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ComboBox mainGenreBox;
        private System.Windows.Forms.Button requestButton;
        private System.Windows.Forms.Button genreButton;
        private System.Windows.Forms.TextBox resultBox;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.CheckBox IDCheckBox;
        private System.Windows.Forms.CheckBox GenreCheckBox;
        private System.Windows.Forms.Label mainGenreLabel;
    }
}

